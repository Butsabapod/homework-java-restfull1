package com.example.demo;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserCrudController {
		@Autowired UserRepository userRepo;
	
		@GetMapping("/addUserTest")
		   @ResponseBody
		   String addUserTest() {
			User user = new User();
			user.setEmail("but1@gmail.com");
			user.setName("But1");
			userRepo.save(user);
		       return "Uesr Added";
		   }
		@GetMapping("/selectUserTest")
		   @ResponseBody
		   String selectUserTest() {
			Iterable<User> users =  userRepo.findAll();
			String out = "";
			for (User user : users) {
				out = out + user.getName();
			}
		       return "Finish Select user " + out;
		   }
		@GetMapping("/updateUserTest")
		   @ResponseBody
		   String updateUserTest() {
			User user = userRepo.findById(4).get();
			String userDetail = "name :" +user.getName() + " Email : "+user.getEmail();
			user.setName("NewBut");
			user.setEmail("newBut@gmail.com");
			userRepo.save(user);
			
		       return "Finish update user " +userDetail;
		   }
		@GetMapping("/deleteUserTest")
		   @ResponseBody
		   String deleteUserTest() {
			if(!userRepo.findById(6).isEmpty()) {
				User user = userRepo.findById(6).get();
				userRepo.delete(user);
			}
			
		       return "Finish delete user ";
		   }
}
