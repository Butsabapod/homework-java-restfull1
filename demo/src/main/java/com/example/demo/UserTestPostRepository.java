package com.example.demo;

import org.springframework.data.repository.CrudRepository;

public interface UserTestPostRepository extends CrudRepository<UserTestPost, Integer> {

}
