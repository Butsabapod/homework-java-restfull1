package com.example.demo;


import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class SimpleController {
	@RequestMapping("/")
	   @ResponseBody
	   String home() {
	       return "Hello World!";
	   }
	@RequestMapping("/test")
	   @ResponseBody
	   String test() {
	       return "Hello World!...Test";
	   }
	@GetMapping("/simple")
	   @ResponseBody
	   String simple() {
	       return "Simple!";
	   }
	@PostMapping("/add")
	   @ResponseBody
	   String add() {
	       return "Hello add";
	   }
	
	@PutMapping("/update")
	  @ResponseBody
	  String update() {
		  return "Hello update";
	  }
	  
	@DeleteMapping("/delete")
	  @ResponseBody
	  String delete() {
		  return "Hello Delete";
	  }
	@GetMapping(path = "/students/{student_id}")
	@ResponseBody
   	public String getGrade(@PathVariable("student_id") String studentId) {
		return "The student id is "+studentId;
   	}
	
	@GetMapping(path = "/students/{group_id}/{student_id}")
	@ResponseBody
   	public String getGrade(@PathVariable("group_id") String groupId , @PathVariable("student_id") String studentId ) {
		return "Group : "+groupId+"\n"+"The student id is "+studentId;
   	}
	
	@GetMapping(path = "/students2/{student_id}")
	@ResponseBody
	public String getGrade2(@PathVariable("student_id") String studentId,
				@RequestParam("semester") String semester,
				@RequestParam("year") String year){
		return "The student id is "+studentId + " request param semester " +semester +"\n"+"Year : "+year;
   	}
	
	@GetMapping(path = "/students3/{student_id}")
   	public ResponseEntity<String> getGrade3(@PathVariable("student_id") String studentId,
                                   						@RequestParam("semester") String semester) {
       		return new ResponseEntity<String>(studentId + " has got an A.", HttpStatus.OK);
   	}
	
	@PostMapping("/testPost")
	   @ResponseBody
	   String testPost() {
	       return "Test Post!";
	   }
	@PostMapping("/testPost/{student_id}")
	   @ResponseBody
	   public String postGrade(@PathVariable("student_id") String studentId,
				@RequestParam("username") String username,
				@RequestParam("password") String password){
		return "Username : "+username +"\n"+"Password : "+password;
  	}
	@PostMapping(path = "/testPost1",consumes = {MediaType.APPLICATION_JSON_VALUE})
	   @ResponseBody
	   String testPost1(@RequestBody UserTestPost user) {
	       return "Username : "+ user.getUsername() +"\n"+"Password :"+user.getPassword() ;
	   }
}
