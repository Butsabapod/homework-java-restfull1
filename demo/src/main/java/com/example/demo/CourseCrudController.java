package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseCrudController {
	@Autowired CourseRepository courseRepo;
	
	@GetMapping("/addCourseTest")
	   @ResponseBody
	   String addCourseTest() {
		Course course = new Course();
		course.setName("Java Course");
		course.setCredit("Aj.oaK");
		courseRepo.save(course);
	       return "Course Added";
	   }
}
