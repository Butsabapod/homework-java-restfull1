package com.example.demo;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired UserTestPostRepository userRepo;
	@Autowired SaleRepository saleRepo;
	
	ArrayList<UserTestPost> userList = new ArrayList<UserTestPost>();


	@GetMapping("/users")
	@ResponseBody
	String getAllUsers() {
		Iterable<UserTestPost> users =  userRepo.findAll();
		String out = "";
		for (UserTestPost user : users) {
			out = out + user.getUsername();
		}
		
		return "Get User " + out;
	}

	@PostMapping(path = "/users", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addUser(@RequestBody UserTestPost user ) {
//		// Add address detail ใช้แค่ userRepo.save(user);มีค่าเท่ากัน
//		Address address = new Address();
//		address.setAddressNumber(user.getAddress().getAddressNumber());
//		address.setProvince(user.getAddress().getRoad());
//		address.setRoad(user.getAddress().getProvince());
//		//
//		user.setAddress(address);
		
		//comment ด้านบน มีค่าเท่ากัน userRepo.save(user);
		Set<Sale>sales = new  HashSet<Sale>();
		Sale sales1 = new Sale();
		sales1.setName("But1");
		sales1.setSalary(5000);
		sales.add(sales1);
		
		Sale sales2 = new Sale();
		sales2.setName("But2");
		sales2.setSalary(4000);
		sales.add(sales2);
		
		userRepo.save(user);
		sales1.setUser(user);
		sales2.setUser(user);
		saleRepo.save(sales1);
		saleRepo.save(sales2);
		user.setSales(sales);
		userRepo.save(user);
		return "Add User " + user.getUsername() + " password " + user.getPassword();
	}

	@PutMapping(path = "/users/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateUser(@PathVariable("Id") int Id, @RequestBody UserTestPost user) {
		//1. Get User ID
		UserTestPost userFormDB = userRepo.findById(Id).get();
		//2. Assign New Value
		userFormDB.setUsername(user.getUsername());
		userFormDB.setPassword(user.getPassword());
		userFormDB.setAge(user.getAge());
		//3. Update the new value
		userRepo.save(userFormDB);
		
		return "Update User " + Id;
	}

	@DeleteMapping("/users/{Id}")
	@ResponseBody
	String deleteUser(@PathVariable("Id") int Id) {
		//1. Get User ID
		UserTestPost userFormDB =  userRepo.findById(Id).get();
		//2. delete value
		userRepo.delete(userFormDB);
		
		
		return "Delete User " + Id;
	}
}